import { clientsArr } from "./tS-clientsArr";

class Bank {
    name: string;
    surname: string;
    active: boolean;
    registration: string;
    cart: any;
    client: clientsArr[];

    constructor(client: any) {
        this.name = client.name;
        this.surname = client.surname;
        this.active = client.active;
        this.registration = client.registration;
        this.cart = client.cart;
        this.client = clientsArr;
    }

    sumAllMoney() {
        let sum: number = 0;
        for (let i: number = 0; i < this.client.length; i++) {
            for (let j: number = 0; j < this.client[i].cart.length; j++) {
                sum += this.client[i].cart[j].countMoney;
                this.client[i].cart[j].history.forEach((item: any) => {
                    sum += item.money;
                });

                if (this.client[i].cart[j].typeCart === 'credit') {
                    // @ts-ignore
                    sum += this.client[i].cart[j].creditLimit;
                }
            }
        }

        return sum;
    }

    sumDebt() {
        let sum: number = 0;

        this.client.forEach(person => {
            person.cart.forEach((cart: any) => {
                if (cart.typeCart === 'credit') {
                    sum += cart.creditCountMoney;
                }
            })
        });

        return sum;
    }

    getCountDebt(callback: (value: number)=> boolean): number {
        let sum = 0;

        let clientDebt = this.client.filter((person: any) => {
            return callback(person.isActive);
        });
        clientDebt.forEach(person => {
            person.cart.forEach((cart: any) => {
                if (cart.typeCart === 'credit') {
                    sum += cart.creditCountMoney;
                }
            })
        });

        return sum;
    }

    getCurrency(currency: any): Promise<number> {
        return fetch("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                    let currencyData: any = {};
                    data.forEach((item: number, index: number) => {
                        currencyData[data[index].ccy] = data[index].buy;
                    });
                    return currencyData[currency];
                }
            )
            // @ts-ignore
            .catch(error => ('error', error));
    }

    getConvert(currency: any): Promise<number> {
        let value = this.getCurrency(currency);

        let money = 0;
        return value.then(data => {
            money = this.sumAllMoney() / Number(data);
            return parseFloat(money.toFixed(2));
        });
    }
}
const bank = new Bank(clientsArr);

