let gulp = require('gulp');
let rename = require('gulp-rename');
let sass = require('gulp-sass')(require('node-sass'));
let ts = require('gulp-typescript');
let autoprefixer = require('gulp-autoprefixer');
let sourcemaps = require('gulp-sourcemaps');
let browserSync = require('browser-sync').create();

function copy(done) {
    gulp.src('./app/style/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .on('error', console.error.bind(console))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 versions'],
            cascade: false
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./app/css'))
        .pipe(browserSync.stream({match: '/*.css'}));
    done();
}

function tsCopy(done) {
    gulp.src('./app/src/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            target: "es6",
        }))
        .pipe(gulp.dest('./app/dist'))
        .pipe(browserSync.stream({match: '/*.js'}));

    done();
}

function sync(done) {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 80,
        rewriteRules: [
            {
                match: /Content-Security-Policy/,
                fn: function (match) {
                    return "DISABLED-Content-Security-Policy";
                }
            }
        ],
    });
    gulp.watch(['./ts//*.ts'], gulp.series(tsCopy));
    gulp.watch(['./style//*.scss'], gulp.series(copy));
    gulp.watch("./*").on('change', browserSync.reload);
    done();
}

exports.copy = copy;
exports.default = sync;
exports.tsCopy = tsCopy;
